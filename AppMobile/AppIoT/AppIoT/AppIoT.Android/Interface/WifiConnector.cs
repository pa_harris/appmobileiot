﻿using Android.App;
using Android.Content;
using Android.Net;
using Android.Net.Wifi;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppIoT.Droid.Interface;
using AppIoT.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
[assembly: Xamarin.Forms.Dependency(typeof(WifiConnector))]
namespace AppIoT.Droid.Interface
{
    public class WifiConnector : IWifiConnector
    {
        public Context context { get; set; }
        public WifiConnector()
        {
            this.context = Android.App.Application.Context;
        }
        public void Connect(string ssid, string password)
        {
            
        }

        [Obsolete]
        public string CurrentSSID()
        {
            WifiManager wifiManager = (WifiManager)(context.GetSystemService(Context.WifiService));

            if (wifiManager != null && !string.IsNullOrEmpty(wifiManager.ConnectionInfo.SSID))
            {
                //IList<WifiConfiguration> myWifis = wifiManager.ConfiguredNetworks;
                //string ssid = string.IsNullOrEmpty(myWifis.FirstOrDefault((o) => o.NetworkId == wifiManager.ConnectionInfo.NetworkId)?.Ssid) ?
                //    wifiManager.ConnectionInfo.SSID :
                //    myWifis.FirstOrDefault((o) => o.NetworkId == wifiManager.ConnectionInfo.NetworkId)?.Ssid;
                return wifiManager.ConnectionInfo?.SSID;
            }

            //var connectivityManager = (ConnectivityManager)Android.App.Application.Context.GetSystemService(Context.ConnectivityService);
            //NetworkInfo networkInfo = connectivityManager.GetNetworkInfo(ConnectivityType.Wifi);
            //if (networkInfo.IsConnected)
            //{
            //    WifiManager wifiManager = (WifiManager)Android.App.Application.Context.GetSystemService(Context.WifiService);
            //    WifiInfo wifiInfo = wifiManager.ConnectionInfo;
            //    string name = networkInfo.ExtraInfo;
            //    string ssid = "\"" + wifiInfo.SSID + "\"";
            //}

            return null;
        }

        public void OpenWifiSetting()
        {
            Xamarin.Forms.Forms.Context.StartActivity(new Android.Content.Intent(Android.Provider.Settings.ActionWifiSettings));
        }
    }
}