﻿using System;
using System.Collections.Generic;
using System.Linq;
using FFImageLoading.Forms.Platform;
using Foundation;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using ProgressRingControl.Forms.Plugin.iOS;
using UIKit;

namespace AppIoT.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Rg.Plugins.Popup.Popup.Init();
            global::Xamarin.Forms.Forms.Init();

            #region Init library
            GoogleClientManager.Initialize();
            CachedImageRenderer.Init();
            ProgressRingRenderer.Init();
            #endregion

            App.screenHeight = (int)UIScreen.MainScreen.Bounds.Height;
            App.screenWidth = (int)UIScreen.MainScreen.Bounds.Width;

            LoadApplication(new App());

            FacebookClientManager.Initialize(app, options);

            return base.FinishedLaunching(app, options);
        }
        public override void OnActivated(UIApplication uiApplication)
        {
            base.OnActivated(uiApplication);
            FacebookClientManager.OnActivated();
        }
        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            FacebookClientManager.OpenUrl(app, url, options);
            GoogleClientManager.OnOpenUrl(app, url, options);
            return true;
        }
        public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            FacebookClientManager.OpenUrl(application, url, sourceApplication, annotation);
            return true;
        }
    }
}
