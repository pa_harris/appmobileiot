﻿using AppIoT.Interface;
using AppIoT.iOS.Interface;
using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;
[assembly: Xamarin.Forms.Dependency(typeof(WifiConnector))]
namespace AppIoT.iOS.Interface
{
    public class WifiConnector : IWifiConnector
    {
        public WifiConnector()
        { }
        public void Connect(string ssid, string password)
        {
            throw new NotImplementedException();
        }

        public string CurrentSSID()
        {
            throw new NotImplementedException();
        }

        public void OpenWifiSetting()
        {
            var WiFiURL = new NSUrl("prefs:root=WIFI");
            if (UIApplication.SharedApplication.CanOpenUrl(WiFiURL))
            {   //Pre iOS 10
                UIApplication.SharedApplication.OpenUrl(WiFiURL);
            }
            else
            {   //iOS 10
                UIApplication.SharedApplication.OpenUrl(new NSUrl("App-Prefs:root=WIFI"));
            }
        }
    }
}