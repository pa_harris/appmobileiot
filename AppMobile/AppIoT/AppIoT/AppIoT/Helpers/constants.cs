﻿using AppIoT.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace AppIoT.Helpers
{
    public static class constants
    {
#if DEBUG
        public static string DomainAPIUrl = "http://akashic-server.xyz";
#else
        public static string DomainAPIUrl = "http://akashic-server.xyz";
        //public static string DomainAPIUrl = "http://6b116940e6d8.ngrok.io";
#endif

        public static string DomainImageUrl = "http://ihi.vn";

        public static string AccessToken { get => Preferences.Get("accessTokenAkashiccorp", ""); set { Preferences.Set("accessTokenAkashiccorp", value); } }
        
        public static UserInfoViewModel Account { get; set; }

        #region Link API
        public static string ApiUserInfo = $"{DomainAPIUrl}/api/Account/UserInfo";
        public static string ApiResendOtp = $"{DomainAPIUrl}/api/Account/ResendOtp";
        public static string ApiRegister = $"{DomainAPIUrl}/api/Account/Register";
        public static string ApiExternalLogins = $"{DomainAPIUrl}/api/Account/ExternalLogins";
        public static string ApiRegisterConfirmOtp = $"{DomainAPIUrl}/api/Account/RegisterConfirmOtp";
        public static string ApiChangePassword = $"{DomainAPIUrl}/api/Account/ChangePassword";
        public static string ApiForgotPassword = $"{DomainAPIUrl}/api/Account/ForgotPassword";
        public static string ApiResetPassword = $"{DomainAPIUrl}/api/Account/ResetPassword";

        #endregion

        public static string firebaseUrl = "";
    }
}
