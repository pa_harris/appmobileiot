﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace AppIoT.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToUniversalTime();
            return dtDateTime;
        }
    }
    public static class NavigationHelper
    {
        public static void RemoveAllPageBefore()
        {
            int count = Xamarin.Forms.Application.Current.MainPage.Navigation.NavigationStack.Count;
            for (int i = 0; i < count - 1; i++)
            {
                Xamarin.Forms.Application.Current.MainPage.Navigation.RemovePage(Xamarin.Forms.Application.Current.MainPage.Navigation.NavigationStack[0]);
            }
        }
    }
    public static class CollectionHelper
    {
        public static void AddList<T>(ObservableCollection<T> des, List<T> source)
        {
            if (source != null)
            {
                foreach (var item in source)
                {
                    des.Add(item);
                }
            }
        }
    }
    public static class CryptographyHelper
    {
        public static string signSHA256(string message, string key)
        {
            byte[] keyByte = Encoding.UTF8.GetBytes(key);
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                string hex = BitConverter.ToString(hashmessage);
                hex = hex.Replace("-", "").ToLower();
                return hex;
            }
        }
    }
    public static class RandomGenerator
    {
        public static Random random;
        public static int RandomNumber(int min, int max)
        {
            if (random == null)
            {
                random = new Random();
            }
            return random.Next(min, max);
        }
        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            if (random == null)
            {
                random = new Random();
            }
            char ch;
            for (int i = 0; i < size; i++)
            {
                int isIntNumeric = RandomNumber(0, 2);
                bool isNumeric = (isIntNumeric == 0) ? false : true;
                if (isNumeric)
                {
                    ch = Convert.ToChar(Convert.ToInt32(Math.Floor(10 * random.NextDouble() + 48)));
                    while (builder.ToString().Contains(ch))
                    {
                        ch = Convert.ToChar(Convert.ToInt32(Math.Floor(10 * random.NextDouble() + 48)));
                    }
                }
                else
                {
                    ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                    while (builder.ToString().Contains(ch))
                    {
                        ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                    }
                }

                builder.Append(ch);
            }
            if (lowerCase)
            {
                return builder.ToString().ToLower();
            }
            return builder.ToString();
        }
    }
    public static class PropertyCopier<TParent, TChild> where TParent : class
                                            where TChild : class
    {
        public static void Copy(TParent parent, TChild child)
        {
            if (parent != null && child != null)
            {
                var parentProperties = parent.GetType().GetProperties();
                var childProperties = child.GetType().GetProperties();

                foreach (var parentProperty in parentProperties)
                {
                    var isvirtualParent = parentProperty.GetMethod.IsVirtual;
                    if (isvirtualParent)
                        continue;
                    foreach (var childProperty in childProperties)
                    {
                        var isvirtualchildren = childProperty.GetMethod.IsVirtual;
                        if (isvirtualchildren)
                            continue;
                        if (parentProperty.Name == childProperty.Name && parentProperty.PropertyType == childProperty.PropertyType)
                        {
                            try
                            {
                                childProperty.SetValue(child, parentProperty.GetValue(parent));
                            }
                            catch { }
                            break;
                        }
                    }
                }
            }
            else
            {
                return;
            }
        }
    }
    public static class ModelStateHelper
    {
        public class ErrorRespone
        {
            public string Message { get; set; }
            public object ModelState { get; set; }
        }
        public static string DesearializeModelStateError(ErrorRespone error)
        {
            if (error != null)
            {
                List<string> listError = new List<string>();
                string errorResult = "";

                var modelStateString = error.ModelState.ToString();
                var listModelStateError = JsonConvert.DeserializeObject<Dictionary<string, object>>(modelStateString);
                foreach (var item in listModelStateError)
                {
                    listError.AddRange(JsonConvert.DeserializeObject<string[]>(item.Value.ToString()));
                }
                foreach (var item in listError)
                {
                    errorResult += item + " ";
                }
                return errorResult;
            }
            return "";
        }
    }
}
