﻿using AppIoT.Helpers;
using AppIoT.Views;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;

namespace AppIoT
{
    public partial class App : Xamarin.Forms.Application
    {
        public static int screenHeight, screenWidth;
        public App()
        {
            Xamarin.Forms.Application.Current.On<Xamarin.Forms.PlatformConfiguration.Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
            InitializeComponent();
            if (Device.RuntimePlatform == Device.iOS)
            {
                MainPage = new NavigationPage(new SplashPageiOS());
                //MainPage = new SplashPageiOS();
            }
            else if (Device.RuntimePlatform == Device.Android)
            {
                if (constants.Account != null)
                {
                    MainPage = new NavigationPage(new HomePage());
                }
                else
                {
                    MainPage = new NavigationPage(new Views.LoginPage());
                }
                //MainPage = new NavigationPage(new Views.LoginPage());
            }
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
