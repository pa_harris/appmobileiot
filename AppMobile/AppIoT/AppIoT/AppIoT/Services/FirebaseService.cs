﻿using AppIoT.Helpers;
using AppIoT.Models;
using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AppIoT.Services
{
    public class FirebaseService
    {
        FirebaseClient client;
        public FirebaseService(string dbUrl = "https://akashicapp-default-rtdb.firebaseio.com/")
        {
            client = new FirebaseClient(dbUrl);
        }
        public ObservableCollection<DeviceInfo> GetAllDevice(string userId, Action<string> callback = null)
        {
            try
            {
                //var observable = client.Child("ba57f327-9c8a-4ed3-83d6-d57d5c04f7e1").AsObservable<DeviceInfo>();
                var observable = client.Child($"{userId}").AsObservable<DeviceInfo>();
                var disposable = observable.Subscribe((d) =>
                {
                    Console.WriteLine(d.Key);
                });
                return observable.AsObservableCollection();
            }
            catch (Exception ee)
            {
                if (callback != null)
                {
                    callback(ee.Message);
                }
            }
            return null;
        }
        public async System.Threading.Tasks.Task<object> GetDeviceAsync(string userId, string deviceId, Action<string, object> callback = null)
        {
            try
            {
                var deviceReceiver = client.Child($"{userId}/{deviceId}").AsObservable<object>().Subscribe((d) =>
                {
                    Console.WriteLine(d.Key);
                    callback(d.Key, d.Object);
                });
                if (deviceId.Contains("robot"))
                {
                    var device = await client.Child($"{userId}").OnceAsync<RobotInfo>();
                    return device.FirstOrDefault(x => x.Object.DeviceId == deviceId).Object;
                }
                else
                {
                    var device = await client.Child($"{userId}").OnceAsync<DeviceInfo>();
                    return device.FirstOrDefault(x => x.Object.DeviceId == deviceId).Object;
                }
            }
            catch (Exception ee)
            {
                //if (callback != null)
                //{
                //    callback(ee.Message);
                //}
            }
            return null;
        }
        public List<string> GetAllDeviceId(string userId, Action<string> callback = null)
        {
            return new List<string>();
        }
        public async System.Threading.Tasks.Task<bool> DeleteDeviceAsync(string userId, string deviceId, Action<string> callback = null)
        {
            try
            {
                await client.Child($"{userId}/{deviceId}").DeleteAsync();
                return true;
            }
            catch (Exception ee)
            {
            }
            return false;
        }
        public async System.Threading.Tasks.Task<bool> AddDeviceAsync(DeviceInfo deviceInfo, Action<string> callback)
        {
            if (string.IsNullOrEmpty(deviceInfo.DeviceId))
            {
                callback("DeviceId không được trống");
                return false;
            }
            if (string.IsNullOrEmpty(deviceInfo.name))
            {
                callback("DeviceName không được trống");
                return false;
            }
            try
            {
                if (deviceInfo.DeviceId.Contains("robot"))
                {
                    RobotInfo robotInfo = new RobotInfo(deviceInfo);
                    await client.Child($"{constants.Account.Id}/{deviceInfo.DeviceId}").PutAsync(robotInfo);
                }
                else
                {
                    await client.Child($"{constants.Account.Id}/{deviceInfo.DeviceId}").PutAsync(deviceInfo);
                }
                
                return true;
            }
            catch (Exception ee)
            {
                callback(ee.Message);
            }
            return false;
        }
        public async System.Threading.Tasks.Task<bool> UpdateDB(string path, object value, Action<string> callback = null)
        {
            try
            {
                await client.Child($"{path}").PutAsync(value);

                return true;
            }
            catch (Exception ee)
            {
                if (callback != null)
                {
                    callback(ee.Message);
                }
            }
            return false;
        }
    }
}
