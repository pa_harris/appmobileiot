﻿using AppIoT.Models;
using AppIoT.Utilities;
using AppIoT.Views;
using AppIoT.Views.Popups;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class ForgotPasswordPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickResetPassword { get; set; }
        public ICommand OnClickOpenOTP { get; set; }
        #endregion

        #region Properties
        private OTPConfirmationPage otpPage;
        private ForgotPasswordPage _MainWindow;
        public ForgotPasswordPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private string _Email;
        public string Email { get => _Email; set { _Email = value; OnPropertyChanged(); } }
        #endregion

        public ForgotPasswordPageViewModel(ForgotPasswordPage mainWindow, string email)
        {
            MainWindow = mainWindow;
            Email = email;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickResetPassword = new Command(() =>
            {
                Rg.Plugins.Popup.Services.PopupNavigation.PushAsync(new CheckEmailPopup(this));
            });
            OnClickOpenOTP = new Command(async () =>
            {
                IsBusy = true;
                Rg.Plugins.Popup.Services.PopupNavigation.PopAllAsync();
                bool isSentOTP = await AccountUtilities.ForgotPassword(new Models.ForgotPasswordBindingModel { Email = this.Email }, (error) =>
                {
                    MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                    IsBusy = false;
                    return;
                });
                if (isSentOTP)
                {
                    RegisterModel.Email = this.Email;
                    await MainWindow.DisplayAlert("Thành công", "Gửi OTP thành công! Vui lòng kiểm tra trong hộp thư!", "OK");
                    if (otpPage == null || otpPage.ViewModel == null)
                    {
                        otpPage = new OTPConfirmationPage();
                    }
                    else
                    {
                        if (otpPage.ViewModel.Email != Email)
                        {
                            otpPage.ViewModel.Email = Email;
                        }
                    }
                    LoadPage(otpPage);
                }
                else
                {
                    MainWindow.DisplayAlert("Cảnh báo", "Có lỗi xảy ra! Vui lòng quay lại sau.", "OK");
                    IsBusy = false;
                    return;
                }
            });
        }
    }
}
