﻿using AppIoT.Helpers;
using AppIoT.Models;
using AppIoT.Views;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class UserInfoPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickLogout { get; set; }
        #endregion

        #region Properties
        private UserInfoPage _MainWindow;
        public UserInfoPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private UserInfoViewModel _Account;
        public UserInfoViewModel Account { get => _Account; set { _Account = value; OnPropertyChanged(); } }
        #endregion

        public UserInfoPageViewModel(UserInfoPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
            Account = constants.Account;
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickLogout = new Command(() =>
            {
                IsBusy = true;
                constants.AccessToken = null;
                constants.Account = null;
                CrossFacebookClient.Current.Logout();
                CrossGoogleClient.Current.Logout();
                LoadPage(new LoginPage());
                NavigationHelper.RemoveAllPageBefore();
            });

        }
    }
}
