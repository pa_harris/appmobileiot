﻿using AppIoT.Helpers;
using AppIoT.Models;
using AppIoT.Services;
using AppIoT.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class ConnectionStatusPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickAddDevice { get; set; }
        #endregion

        #region Properties
        private ConnectionStatusPage _MainWindow;
        public ConnectionStatusPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private bool _IsConnected;
        public bool IsConnected { get => _IsConnected; set { _IsConnected = value; OnPropertyChanged(); } }
        private string _DeviceName;
        public string DeviceName { get => _DeviceName; set { _DeviceName = value; OnPropertyChanged(); } }
        private EDeviceType _DeviceType;
        public EDeviceType DeviceType { get => _DeviceType; set { _DeviceType = value; OnPropertyChanged(); } }
        #endregion

        public ConnectionStatusPageViewModel(ConnectionStatusPage mainWindow, EDeviceType deviceType, bool isConnected)
        {
            MainWindow = mainWindow;
            IsConnected = isConnected;
            DeviceType = deviceType;
            DeviceName = deviceType == EDeviceType.Switch ? "Công tắc 1" : (deviceType == EDeviceType.Socket) ? "Ổ cắm 1" : "Robot 1";
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
            //Xamarin.Forms.Application.Current.MainPage.Navigation.RemovePage(Xamarin.Forms.Application.Current.MainPage.Navigation.NavigationStack[Xamarin.Forms.Application.Current.MainPage.Navigation.NavigationStack.Count - 2]);

        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickAddDevice = new Command(async () =>
            {
                IsBusy = true;
                AddNewDeviceModel.DeviceName = DeviceName;
                if (ValidateInfo())
                {
                    DeviceInfo device = new DeviceInfo();
                    device.DeviceId = AddNewDeviceModel.DeviceId;
                    device.name = AddNewDeviceModel.DeviceName;

                    Thread.Sleep(TimeSpan.FromSeconds(2));
                    FirebaseService firebaseService = new FirebaseService();
                    bool isAdded = await firebaseService.AddDeviceAsync(device, (o) =>
                    {
                        MainWindow.DisplayAlert("Thông báo", o, "OK");
                    });
                    if (isAdded)
                    {
                        LoadPage(new HomePage());
                        NavigationHelper.RemoveAllPageBefore();
                    }
                }
                IsBusy = false;
            });
        }

        private bool ValidateInfo()
        {
            if (string.IsNullOrEmpty(AddNewDeviceModel.DeviceId))
            {
                MainWindow.DisplayAlert("Thông báo", "DeviceId không được trống.", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(AddNewDeviceModel.DeviceName))
            {
                MainWindow.DisplayAlert("Thông báo", "DeviceName không được trống.", "OK");
                return false;
            }
            if (!AddNewDeviceModel.DeviceId.Contains("socket") && !AddNewDeviceModel.DeviceId.Contains("switch") && !AddNewDeviceModel.DeviceId.Contains("robot"))
            {
                MainWindow.DisplayAlert("Thông báo", "DeviceId không đúng định dạng.", "OK");
                return false;
            }
            return true;
        }
    }
}
