﻿using AppIoT.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace AppIoT.ViewModels
{
    public class RobotControlPageViewModel : BaseViewModel
    {
        #region Commands
        
        #endregion

        #region Properties
        private RobotControlPage _MainWindow;
        public RobotControlPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        
        #endregion

        public RobotControlPageViewModel(RobotControlPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            
        }
    }
}
