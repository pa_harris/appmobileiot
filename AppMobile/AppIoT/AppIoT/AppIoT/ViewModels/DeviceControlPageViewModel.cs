﻿using AppIoT.Helpers;
using AppIoT.Models;
using AppIoT.Services;
using AppIoT.Views;
using FFImageLoading.Svg.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class DeviceControlPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickSetTimer { get; set; }
        public ICommand OnClickDeleteDevice { get; set; }
        public ICommand OnClickOnOff { get; set; }
        public ICommand OnClickBack { get; set; }
        public ICommand OnClickRobotFunction { get; set; }
        public ICommand OnClickRobotNavigation { get; set; }
        #endregion

        #region Properties
        FirebaseService firebaseService = new FirebaseService();
        private DeviceControlPage _MainWindow;
        public DeviceControlPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        bool isFirstLoad = true;
        private string _DeviceId;
        public string DeviceId { get => _DeviceId; set { _DeviceId = value; OnPropertyChanged(); } }
        private EDeviceType _DeviceType;
        public EDeviceType DeviceType { get => _DeviceType; set { _DeviceType = value; OnPropertyChanged(); } }
        private DeviceInfo _Devicee = new DeviceInfo();
        public DeviceInfo Devicee { get => _Devicee; set { _Devicee = value; OnPropertyChanged(); } }
        private RobotInfo _Robot = new RobotInfo();
        public RobotInfo Robot { get => _Robot; set { _Robot = value; OnPropertyChanged(); } }
        private string _RobotStatusText;
        public string RobotStatusText { get => _RobotStatusText; set { _RobotStatusText = value; OnPropertyChanged(); } }
        #endregion

        public DeviceControlPageViewModel(DeviceControlPage mainWindow, string deviceId)
        {
            isFirstLoad = true;
            MainWindow = mainWindow;
            DeviceId = deviceId;
            DeviceType = deviceId.Contains("robot") ? EDeviceType.Robot : deviceId.Contains("switch") ? EDeviceType.Switch : EDeviceType.Socket;
            FirstLoad();
            Thread.Sleep(TimeSpan.FromSeconds(10));
            isFirstLoad = false;
        }
        private async void FirstLoad()
        {
            LoadCommand();
            if (DeviceType == EDeviceType.Robot)
            {
                var robotInfo = await firebaseService.GetDeviceAsync(constants.Account.Id, DeviceId, ReloadDevice);
                PropertyCopier<object, RobotInfo>.Copy(robotInfo, Robot);
                OnPropertyChanged(nameof(Robot.DeviceId));
                OnPropertyChanged(nameof(Robot.timestamp));
                OnPropertyChanged(nameof(Robot.name));
                OnPropertyChanged(nameof(Robot.status));
                OnPropertyChanged(nameof(Robot.Tramsac));
                OnPropertyChanged(nameof(Robot.clear));
                OnPropertyChanged(nameof(Robot.control));
                OnPropertyChanged(nameof(Robot.fanmode));
                OnPropertyChanged(nameof(Robot.pin));
                OnPropertyChanged(nameof(Robot.stop));
                OnPropertyChanged(nameof(Robot));
            }
            else
            {
                var deviceInfo = await firebaseService.GetDeviceAsync(constants.Account.Id, DeviceId, ReloadDevice);
                PropertyCopier<object, DeviceInfo>.Copy(deviceInfo, Devicee);
                OnPropertyChanged(nameof(Devicee.DeviceId));
                OnPropertyChanged(nameof(Devicee.timestamp));
                OnPropertyChanged(nameof(Devicee.name));
                OnPropertyChanged(nameof(Devicee.status));
                OnPropertyChanged(nameof(Devicee));
            }
        }

        private void ReloadDevice(string key, object value)
        {
            if (!isFirstLoad)
            {
                switch (key)
                {
                    case "Type":
                        if (DeviceType == EDeviceType.Robot)
                        {
                            Robot.Type = (EDeviceType)value;
                            OnPropertyChanged(nameof(Robot.Type));
                        }
                        else
                        {
                            Devicee.Type = (EDeviceType)value;
                            OnPropertyChanged(nameof(Devicee.Type));
                        }                        
                        break;
                    case "name":
                        if (DeviceType == EDeviceType.Robot)
                        {
                            Robot.name = value.ToString();
                            OnPropertyChanged(nameof(Robot.name));
                        }
                        else
                        {
                            Devicee.name = value.ToString();
                            OnPropertyChanged(nameof(Devicee.name));
                        }                        
                        break;
                    case "status":
                        if (DeviceType == EDeviceType.Robot)
                        {
                            Robot.status = value.ToString();
                            OnPropertyChanged(nameof(Robot.status));
                        }
                        else
                        {
                            Devicee.status = value.ToString();
                            OnPropertyChanged(nameof(Devicee.status));
                        }
                        break;
                    case "timestamp":
                        if (DeviceType == EDeviceType.Robot)
                        {
                            Robot.timestamp = (long)value;
                            OnPropertyChanged(nameof(Robot.timestamp));
                        }
                        else
                        {
                            Devicee.timestamp = (long)value;
                            OnPropertyChanged(nameof(Devicee.timestamp));
                        }
                        break;
                    case "Tramsac":
                        Robot.Tramsac = value.ToString();
                        OnPropertyChanged(nameof(Robot.Tramsac));
                        RobotStatusText = (value.Equals("on")) ? "Trạm sạc" : RobotStatusText;
                        break;
                    case "clear":
                        Robot.clear = value.ToString();
                        OnPropertyChanged(nameof(Robot.clear));
                        RobotStatusText = (value.Equals("off")) ? RobotStatusText : value.ToString(); ;
                        break;
                    case "control":
                        Robot.control = value.ToString();
                        OnPropertyChanged(nameof(Robot.control));
                        break;
                    case "fanmode":
                        Robot.fanmode = value.ToString();
                        OnPropertyChanged(nameof(Robot.fanmode));
                        RobotStatusText = (value.Equals("on")) ? "Chế độ quạt" : RobotStatusText;
                        break;
                    case "pin":
                        Robot.pin = value.ToString();
                        OnPropertyChanged(nameof(Robot.pin));
                        break;
                    case "stop":
                        Robot.stop = value.ToString();
                        OnPropertyChanged(nameof(Robot.stop));
                        RobotStatusText = (value.Equals("on")) ? "Tạm dừng" : RobotStatusText;
                        break;
                    default:
                        break;
                }
                OnPropertyChanged(nameof(Devicee));
                OnPropertyChanged(nameof(Robot));
            }
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickSetTimer = new Command(() =>
            {

            });
            OnClickDeleteDevice = new Command(async () =>
            {
                bool isDelete = await MainWindow.DisplayAlert("Thông báo", "Bạn có chắc muốn xóa thiết bị?", "OK", "Cancel");
                if (isDelete)
                {
                    IsBusy = true;
                    await firebaseService.DeleteDeviceAsync(constants.Account.Id, DeviceId);
                    await Application.Current.MainPage.Navigation.PopAsync();
                }
            });
            OnClickOnOff = new Command(async () =>
            {
                await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/status", (Devicee.status.Equals("off")) ? "on" : "off");
            });
            OnClickBack = new Command(async () =>
            {
                await Application.Current.MainPage.Navigation.PopAsync();
            });
            OnClickRobotFunction = new Command<object>(async (o) =>
            {
                var stack = o as StackLayout;
                if (stack?.Children != null && stack?.Children?.Count > 0)
                {
                    var label = stack.Children.FirstOrDefault(p => p.GetType().Equals(typeof(Label))) as Label;
                    if (label != null)
                    {
                        if (label.Text.Contains("Làm sạch"))
                        {
                            // Làm sạch function
                            await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/clear", label.Text);

                            await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/stop", "off");
                            await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/Tramsac", "off");
                            await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/fanmode", "off");
                        }
                        else
                        {
                            switch (label.Text)
                            {
                                case "Tạm dừng":
                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/stop", "on");

                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/clear", "off");
                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/Tramsac", "off");
                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/fanmode", "off");
                                    break;
                                case "Trạm sạc":
                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/Tramsac", "on");

                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/clear", "off");
                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/stop", "off");
                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/fanmode", "off");
                                    break;
                                case "Chế độ quạt":
                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/fanmode", "on");

                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/clear", "off");
                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/stop", "off");
                                    await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/Tramsac", "off");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            });
            OnClickRobotNavigation = new Command<object>(async (o) =>
            {
                if (o != null)
                {
                    string direction = o.ToString();
                    if (Robot.control.Equals("off"))
                    {
                        await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/control", direction);
                    }
                    else
                    {
                        if (Robot.control.Equals(direction))
                        {
                            await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/control", "off");
                        }
                        else
                        {
                            await firebaseService.UpdateDB($"{constants.Account.Id}/{DeviceId}/control", direction);
                        }
                    }                    
                }                
            });
        }
    }
}