﻿using AppIoT.Models;
using AppIoT.Utilities;
using AppIoT.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class RegisterPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickRegister { get; set; }
        #endregion

        #region Properties
        private OTPConfirmationPage otpPage;
        private RegisterPage _MainWindow;
        public RegisterPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private string _Email;
        public string Email { get => _Email; set { _Email = value; OnPropertyChanged(); } }
        private string _Password;
        public string Password { get => _Password; set { _Password = value; OnPropertyChanged(); } }
        private string _ConfirmPassword;
        public string ConfirmPassword { get => _ConfirmPassword; set { _ConfirmPassword = value; OnPropertyChanged(); } }
        #endregion

        public RegisterPageViewModel(RegisterPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickRegister = new Command(async () =>
            {
                IsBusy = true;
                if (ValidateData())
                {
                    bool isDoneRegisterStep1 = await AccountUtilities.Register(new RegisterBindingModel { Email = this.Email }, (error) =>
                    {
                        MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                        IsBusy = false;
                        return;
                    });
                    if (isDoneRegisterStep1)
                    {
                        await MainWindow.DisplayAlert("Thành công", "Gửi OTP thành công! Vui lòng kiểm tra trong hộp thư!", "OK");
                        RegisterModel.Email = this.Email;
                        RegisterModel.Password = this.Password;
                        RegisterModel.ConfirmPassword = this.ConfirmPassword;
                        if (otpPage == null || otpPage.ViewModel == null)
                        {
                            otpPage = new OTPConfirmationPage();
                        }
                        LoadPage(otpPage);
                    }
                    else
                    {
                        MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng thử lại sau!", "OK");
                    }
                }
                IsBusy = false;
            });
        }
        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(Email))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Email không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Mật khẩu không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(ConfirmPassword))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Xác nhận mật khẩu không được trống!", "OK");
                return false;
            }
            if (!Password.Equals(ConfirmPassword))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Mật khẩu không khớp!", "OK");
                return false;
            }
            return true;
        }
    }
}
