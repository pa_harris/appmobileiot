﻿using AppIoT.Helpers;
using AppIoT.Interface;
using AppIoT.Models;
using AppIoT.Services;
using AppIoT.Utilities;
using AppIoT.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class WifiConnectionPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickSentDevice { get; set; }
        public ICommand OnClickOpenWifiSetting { get; set; }
        #endregion

        #region Properties
        private WifiConnectionPage _MainWindow;
        public WifiConnectionPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private EDeviceType _DeviceType;
        public EDeviceType DeviceType { get => _DeviceType; set { _DeviceType = value; OnPropertyChanged(); } }
        private string _SSID;
        public string SSID { get => _SSID; set { _SSID = value; OnPropertyChanged(); } }
        private string _Password;
        public string Password { get => _Password; set { _Password = value; OnPropertyChanged(); } }
        #endregion

        public WifiConnectionPageViewModel(WifiConnectionPage mainWindow, EDeviceType deviceType)
        {
            MainWindow = mainWindow;
            DeviceType = deviceType;
            AddNewDeviceModel.DeviceType = deviceType;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickSentDevice = new Command(() =>
            {
                if (ValidateData())
                {
                    string deviceId = "";
                    switch (DeviceType)
                    {
                        case EDeviceType.Socket:
                            deviceId += "socket";
                            break;
                        case EDeviceType.Switch:
                            deviceId += "switch";
                            break;
                        case EDeviceType.Robot:
                            deviceId += "robot";
                            break;
                        default:
                            return;
                    }

                    FirebaseService firebaseService = new FirebaseService();

                    List<string> listId = firebaseService.GetAllDeviceId(constants.Account.Id);
                    int num = RandomGenerator.RandomNumber(1,999);
                    while (listId.Contains(deviceId + num.ToString("000")))
                    {
                        num = RandomGenerator.RandomNumber(1, 999);
                    }
                    deviceId += num;
                    AddNewDeviceModel.DeviceId = deviceId;

                    LoadPage(new TimerDeviceConnectionPage(new UpdateWifiDeviceModel { SSID = this.SSID, Password = this.Password, DeviceId = deviceId }));
                }
            });
            OnClickOpenWifiSetting = new Command((o) =>
            {
                var wifiConnector = Xamarin.Forms.DependencyService.Get<IWifiConnector>();
                if (wifiConnector != null)
                {
                    wifiConnector.OpenWifiSetting();
                }
            });
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(SSID))
            {
                MainWindow.DisplayAlert("Cảnh báo", "SSID không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Mật khẩu không được trống!", "OK");
                return false;
            }
            return true;
        }
    }
}
