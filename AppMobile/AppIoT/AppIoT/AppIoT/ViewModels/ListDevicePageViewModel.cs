﻿using AppIoT.Models;
using AppIoT.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class DeviceType
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public EDeviceType Type { get; set; }
        public string Icon { get; set; }
    }
    public class ListDevicePageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickChooseDevice { get; set; }
        #endregion

        #region Properties
        private WifiConnectionPage wifiConnectionPage;
        private ListDevicePage _MainWindow;
        public ListDevicePage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private ObservableCollection<DeviceType> _ListDeviceType = new ObservableCollection<DeviceType>();
        public ObservableCollection<DeviceType> ListDeviceType { get => _ListDeviceType; set { _ListDeviceType = value; OnPropertyChanged(); } }
        #endregion

        public ListDevicePageViewModel(ListDevicePage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
            LoadDeviceType();
        }

        private void LoadDeviceType()
        {
            if (ListDeviceType == null)
            {
                ListDeviceType = new ObservableCollection<DeviceType>();
            }
            ListDeviceType.Clear();
            ListDeviceType.Add(new DeviceType { Id = "socket", Name = "Ổ cắm", Type = EDeviceType.Socket, Icon = "resource://AppIoT.Resources.SVG.socket.svg" });
            ListDeviceType.Add(new DeviceType { Id = "switch", Name = "Công tắc", Type = EDeviceType.Switch, Icon = "resource://AppIoT.Resources.SVG.switch.svg" });
            ListDeviceType.Add(new DeviceType { Id = "robot", Name = "Robot lau nhà", Type = EDeviceType.Robot, Icon = "resource://AppIoT.Resources.SVG.robot.svg" });
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickChooseDevice = new Command<object>((o) =>
            {
                IsBusy = true;
                if (o != null)
                {
                    DeviceType deviceType = o as DeviceType;
                    if (wifiConnectionPage == null || wifiConnectionPage.ViewModel.DeviceType != deviceType.Type)
                    {
                        wifiConnectionPage = new WifiConnectionPage(deviceType.Type);
                    }
                    LoadPage(wifiConnectionPage);
                }
            });
        }
    }
}
