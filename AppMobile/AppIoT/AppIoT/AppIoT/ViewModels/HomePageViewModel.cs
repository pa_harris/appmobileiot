﻿using AppIoT.Helpers;
using AppIoT.Models;
using AppIoT.Services;
using AppIoT.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class HomePageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickUserInfo { get; set; }
        public ICommand OnClickAddDevice { get; set; }
        public ICommand OnClickAddUser { get; set; }
        public ICommand OnClickDevice { get; set; }
        #endregion

        #region Properties
        private UserInfoPage userInfoPage;
        private ListDevicePage listDevicePage;
        private HomePage _MainWindow;
        public HomePage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private UserInfoViewModel _Account;
        public UserInfoViewModel Account { get => _Account; set { _Account = value; OnPropertyChanged(); } }
        private ObservableCollection<DeviceInfo> _Devices = new ObservableCollection<DeviceInfo>();
        public ObservableCollection<DeviceInfo> Devices { get => _Devices; set { _Devices = value; OnPropertyChanged(); } }
        private double _WidthItem;
        public double WidthItem { get => _WidthItem; set { _WidthItem = value; OnPropertyChanged(); } }
        #endregion

        public HomePageViewModel(HomePage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
            this.Account = constants.Account;
            this.WidthItem = App.screenWidth / 2 - 15 - 5;
            LoadDevices();
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickUserInfo = new Command(() =>
            {
                IsBusy = true;
                if (userInfoPage == null)
                {
                    userInfoPage = new UserInfoPage();
                }
                LoadPage(userInfoPage);
            });
            OnClickAddDevice = new Command(() =>
            {
                IsBusy = true;
                if (listDevicePage == null)
                {
                    listDevicePage = new ListDevicePage();
                }
                LoadPage(listDevicePage);
            });
            OnClickAddUser = new Command(() =>
            {
                //LoadPage(new DeviceControlPage("robot712"));
                LoadPage(new DeviceControlPage("socket460"));
            });
            OnClickDevice = new Command<object>((o) =>
            {
                IsBusy = true;
                Task t = new Task(() =>
                {
                    if (o != null)
                    {
                        var dataContext = o as DeviceInfo;
                        string deviceId = dataContext.DeviceId;
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            LoadPage(new DeviceControlPage(deviceId));
                        });
                    }
                });
                t.Start();     
            });
        }
        private void LoadDevices()
        {
            //if (Devices == null)
            //{
            //    Devices = new ObservableCollection<DeviceInfo>();
            //}
            //Devices.Clear();
            //Devices.Add(new DeviceModel { Id = "switch1", Name = "Công tắc 1", Status = EDeviceStatus.Off, Type = EDeviceType.Switch });
            //Devices.Add(new DeviceModel { Id = "socket2", Name = "Ổ cắm 2", Status = EDeviceStatus.On, Type = EDeviceType.Socket });
            //Devices.Add(new DeviceModel { Id = "robot3", Name = "Robot 3", Status = EDeviceStatus.Disconnected, Type = EDeviceType.Robot });
            //Devices.Add(new DeviceModel { Id = "robot4", Name = "Robot 4", Status = EDeviceStatus.Disconnected, Type = EDeviceType.Robot });

            FirebaseService firebaseService = new FirebaseService();
            Devices = firebaseService.GetAllDevice(constants.Account.Id);
            OnPropertyChanged(nameof(Devices));
            //Devices.Add(new DeviceInfo { DeviceId = "socket123", name = "Ổ cắm", status = "on", timestamp = 1234567890 });
        }
    }
}
