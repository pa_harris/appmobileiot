﻿using AppIoT.Interface;
using AppIoT.Models;
using AppIoT.Utilities;
using AppIoT.Views;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class TimerDeviceConnectionPageViewModel : BaseViewModel
    {
        #region Commands
        #endregion

        #region Properties
        private TimerDeviceConnectionPage _MainWindow;
        public TimerDeviceConnectionPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private double _CurrentTimer = 180;
        public double CurrentTimer { get => _CurrentTimer; set { _CurrentTimer = value; OnPropertyChanged(); } }
        private UpdateWifiDeviceModel _UpdateWifiDeviceModel;
        public UpdateWifiDeviceModel UpdateWifiDeviceModel { get => _UpdateWifiDeviceModel; set { _UpdateWifiDeviceModel = value; OnPropertyChanged(); } }
        private bool _IsFoundDevice = false;
        public bool IsFoundDevice { get => _IsFoundDevice; set { _IsFoundDevice = value; OnPropertyChanged(); } }
        private bool _IsConnected = false;
        public bool IsConnected { get => _IsConnected; set { _IsConnected = value; OnPropertyChanged(); } }

        #endregion

        public TimerDeviceConnectionPageViewModel(TimerDeviceConnectionPage mainWindow, UpdateWifiDeviceModel updateWifiDeviceModel)
        {
            MainWindow = mainWindow;
            UpdateWifiDeviceModel = updateWifiDeviceModel;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                if (CurrentTimer > 0)
                {
                    CurrentTimer--;
                    OnPropertyChanged(nameof(CurrentTimer));
                    if (Application.Current.MainPage.Navigation.NavigationStack[Application.Current.MainPage.Navigation.NavigationStack.Count - 1].GetType().ToString().Contains("TimerDeviceConnectionPage"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                OnTimeout();
                return false;
            });

            Thread t = new Thread(ConnectingProcess);
            t.Start();
        }

        private async void ConnectingProcess()
        {
            Thread.Sleep(TimeSpan.FromSeconds(2));
            // Finding device
            //var version = Xamarin.Essentials.DeviceInfo.Version;
            //if (version.Major < 10)
            //{
                
            //}
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync<LocationWhenInUsePermission>();
            if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
            {
                if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                {
                    await MainWindow.DisplayAlert("Cảnh báo", "Chấp nhận quyền sử dụng vị trí để sử dụng tính năng này!", "OK");
                }

                status = await CrossPermissions.Current.RequestPermissionAsync<LocationWhenInUsePermission>();
            }
            if (status == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
            {
                var wifiConnector = Xamarin.Forms.DependencyService.Get<IWifiConnector>();
                if (!wifiConnector.CurrentSSID().Contains("akashic"))
                {
                    await MainWindow.DisplayAlert("Thông báo", "Không tìm thấy thiết bị. Vui lòng kết nối lại vào wifi thiết bị.", "OK");
                    Device.BeginInvokeOnMainThread(() => OnTimeout());
                    return;
                }
                IsFoundDevice = true;
            }
            else
            {
                return;
            }

            Thread.Sleep(TimeSpan.FromSeconds(2));
            // Sent data
            bool isSentData = await DeviceUtilities.UpdateWifi(UpdateWifiDeviceModel, (o) =>
            {
                MainWindow.DisplayAlert("Thông báo", o, "OK");
                Device.BeginInvokeOnMainThread(() => OnTimeout());
                return;
            });
            IsConnected = true;
            Device.BeginInvokeOnMainThread(() => LoadPage(new ConnectionStatusPage(AddNewDeviceModel.DeviceType, true)));
        }

        private void OnTimeout()
        {
            LoadPage(new ConnectionStatusPage(AddNewDeviceModel.DeviceType, false));
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
        }
    }
}
