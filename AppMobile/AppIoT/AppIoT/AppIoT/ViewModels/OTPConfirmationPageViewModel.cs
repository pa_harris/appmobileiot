﻿using AppIoT.Helpers;
using AppIoT.Models;
using AppIoT.Utilities;
using AppIoT.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class OTPConfirmationPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickConfirmOTP { get; set; }
        public ICommand OnClickResend { get; set; }
        #endregion

        #region Properties
        private ResetPasswordPage resetPasswordPage;
        private LoginPage loginPage;
        private OTPConfirmationPage _MainWindow;
        public OTPConfirmationPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private string _Email;
        public string Email { get => _Email; set { _Email = value; OnPropertyChanged(); } }
        private bool _IsCanResend;
        public bool IsCanResend { get => _IsCanResend; set { _IsCanResend = value; OnPropertyChanged(); } }
        private string _OTP;
        public string OTP { get => _OTP; set { _OTP = value; OnPropertyChanged(); } }
        public int TimerMinute { get; set; } = 1;
        public int TimerSecond { get; set; } = 30;

        #endregion

        public OTPConfirmationPageViewModel(OTPConfirmationPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
            TimerMinute = 1;
            TimerSecond = 30;
            IsCanResend = false;
            Email = RegisterModel.Email;
            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                if (TimerSecond > 0)
                {
                    TimerSecond--;
                    OnPropertyChanged(nameof(TimerSecond));
                }
                else
                {
                    TimerSecond = 59;
                    OnPropertyChanged(nameof(TimerSecond));
                    TimerMinute--;
                    OnPropertyChanged(nameof(TimerMinute));
                }
                if (TimerMinute == 0 && TimerSecond == 0)
                {
                    IsCanResend = true;
                    return false;
                }
                else
                {
                    return true;
                }
            });
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickConfirmOTP = new Command(async () =>
            {
                IsBusy = true;
                if (!ValidateData())
                {
                    IsBusy = false;
                    return;
                }
                string lastPage = Application.Current.MainPage.Navigation.NavigationStack[Application.Current.MainPage.Navigation.NavigationStack.Count() - 2].GetType().ToString();
                if (lastPage.Contains("ForgotPassword"))
                {
                    RegisterModel.Email = this.Email;
                    RegisterModel.OTP = this.OTP;
                    if (resetPasswordPage == null || resetPasswordPage.ViewModel == null)
                    {
                        resetPasswordPage = new ResetPasswordPage();
                    }
                    else
                    {
                        resetPasswordPage.ViewModel.Email = this.Email;
                        resetPasswordPage.ViewModel.OTP = OTP;
                    }
                    LoadPage(resetPasswordPage);
                }
                else
                {
                    bool isConfirmedOTP = await AccountUtilities.RegisterConfirmOtp(new ConfirmOtpModel
                    {
                        Email = RegisterModel.Email,
                        Otp = OTP,
                        ConfirmPassword = RegisterModel.ConfirmPassword,
                        Password = RegisterModel.Password
                    }, (error) =>
                    {
                        MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                        IsBusy = false;
                        return;
                    });
                    if (isConfirmedOTP)
                    {
                        if (loginPage == null)
                        {
                            loginPage = new LoginPage();
                        }
                        LoadPage(loginPage);
                        MainWindow.DisplayAlert("Thành công", "Đăng kí thành công! Vui lòng đăng nhập để sử dụng ứng dụng!", "OK");
                        NavigationHelper.RemoveAllPageBefore();
                    }
                    else
                    {
                        await MainWindow.DisplayAlert("Cảnh báo", "OTP sai! Vui lòng nhập lại!", "OK");
                        IsBusy = false;
                        return;
                    }
                }
            });
            OnClickResend = new Command<string>(async (o) =>
            {
                IsBusy = true;
                string lastPage = Application.Current.MainPage.Navigation.NavigationStack[Application.Current.MainPage.Navigation.NavigationStack.Count() - 2].GetType().ToString();
                EOTPActionType action = lastPage.Contains("ForgotPassword") ? EOTPActionType.AccountForgotPassword : EOTPActionType.AccountRegister;
                if (!string.IsNullOrEmpty(o))
                {
                    if (o.Contains("Gửi lại."))
                    {
                        // gửi otp
                        bool isResentOTP = await AccountUtilities.ResendOTP(new ResendOtpModel { ActionType = action, Email = RegisterModel.Email }, (error) =>
                        {
                            MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                            IsBusy = false;
                            return;
                        });
                        if (isResentOTP)
                        {
                            MainWindow.DisplayAlert("Thành công", "Gửi OTP thành công! Vui lòng kiểm tra trong hộp thư!", "OK");
                        }
                        else
                        {
                            MainWindow.DisplayAlert("Cảnh báo", "Gửi OTP thất bại! Vui lòng thử lại sau!", "OK");
                        }
                        // reset lbl
                        FirstLoad();
                    }
                }
                IsBusy = false;
            });
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(OTP))
            {
                MainWindow.DisplayAlert("Cảnh báo", "OTP không được trống!", "OK");
                return false;
            }
            if (OTP.Length != 4)
            {
                MainWindow.DisplayAlert("Cảnh báo", "OTP không hợp lệ!", "OK");
                return false;
            }
            return true;
        }
    }
}
