﻿using AppIoT.Models;
using AppIoT.Utilities;
using AppIoT.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppIoT.ViewModels
{
    public class ResetPasswordPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickResetPassword { get; set; }
        #endregion

        #region Properties
        private LoginPage loginPage;
        private ResetPasswordPage _MainWindow;
        public ResetPasswordPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private string _Email;
        public string Email { get => _Email; set { _Email = value; OnPropertyChanged(); } }
        private string _OTP;
        public string OTP { get => _OTP; set { _OTP = value; OnPropertyChanged(); } }
        private string _Password;
        public string Password { get => _Password; set { _Password = value; OnPropertyChanged(); } }
        private string _ConfirmPassword;
        public string ConfirmPassword { get => _ConfirmPassword; set { _ConfirmPassword = value; OnPropertyChanged(); } }
        #endregion

        public ResetPasswordPageViewModel(ResetPasswordPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
            Email = RegisterModel.Email;
            OTP = RegisterModel.OTP;
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickResetPassword = new Command(async () =>
            {
                IsBusy = true;
                if (ValidateData())
                {
                    bool isResetSucceed = await AccountUtilities.ResetPassword(new ResetPasswordBindingModel
                    {
                        Email = RegisterModel.Email,
                        NewPassword = Password,
                        ConfirmPassword = this.ConfirmPassword,
                        OTP = RegisterModel.OTP
                    }, (error) =>
                    {
                        MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                        IsBusy = false;
                        return;
                    });
                    if (isResetSucceed)
                    {
                        if (loginPage == null)
                        {
                            loginPage = new LoginPage();
                        }
                        LoadPage(loginPage);
                        loginPage.DisplayAlert("Thành công", "Vui lòng đăng nhập để tiếp tục sử dụng.", "OK");
                    }
                    else
                    {
                        MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau.", "OK");
                    }
                }
                IsBusy = false;
            });
        }
        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(Email))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Email không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Mật khẩu không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(ConfirmPassword))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Xác nhận mật khẩu không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(OTP))
            {
                MainWindow.DisplayAlert("Cảnh báo", "OTP không được trống!", "OK");
                return false;
            }
            if (OTP.Length != 4)
            {
                MainWindow.DisplayAlert("Cảnh báo", "OTP phải có 4 chữ số!", "OK");
                return false;
            }
            if (!Password.Equals(ConfirmPassword))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Mật khẩu không khớp!", "OK");
                return false;
            }

            return true;
        }
    }
}
