﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace AppIoT.Converters
{
    public class ClearmodeToSVGSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && parameter != null)
            {
                string clearmode = value.ToString();
                string buttonmode = parameter.ToString();

                if (buttonmode.Equals("Làm sạch thông minh"))
                {
                    if (clearmode.Equals(buttonmode))
                    {
                        return "resource://AppIoT.Resources.SVG.lamsachthongminh-on.svg";
                    }
                    else
                    {
                        return "resource://AppIoT.Resources.SVG.lamsachthongminh-off.svg";
                    }
                }
                else if (buttonmode.Equals("Làm sạch tại chỗ"))
                {
                    if (clearmode.Equals(buttonmode))
                    {
                        return "resource://AppIoT.Resources.SVG.lamsachtaicho-on.svg";
                    }
                    else
                    {
                        return "resource://AppIoT.Resources.SVG.lamsachtaicho-off.svg";
                    }
                }
                else if (buttonmode.Equals("Làm sạch cạnh"))
                {
                    if (clearmode.Equals(buttonmode))
                    {
                        return "resource://AppIoT.Resources.SVG.lamsachcanh-on.svg";
                    }
                    else
                    {
                        return "resource://AppIoT.Resources.SVG.lamsachcanh-off.svg";
                    }
                }
                else if (buttonmode.Contains("Tạm dừng"))
                {
                    if (clearmode.Equals("on"))
                    {
                        return "resource://AppIoT.Resources.SVG.power-on.svg";
                    }
                    else
                    {
                        return "resource://AppIoT.Resources.SVG.power-off.svg";
                    }
                }
                else if (buttonmode.Contains("Trạm sạc"))
                {
                    if (clearmode.Equals("on"))
                    {
                        return "resource://AppIoT.Resources.SVG.battery-on.svg";
                    }
                    else
                    {
                        return "resource://AppIoT.Resources.SVG.battery-off.svg";
                    }
                }
                else if (buttonmode.Contains("Chế độ quạt"))
                {
                    if (clearmode.Equals("on"))
                    {
                        return "resource://AppIoT.Resources.SVG.fan-on.svg";
                    }
                    else
                    {
                        return "resource://AppIoT.Resources.SVG.fan-off.svg";
                    }
                }
                else if (buttonmode.Contains("up") || buttonmode.Contains("down") || buttonmode.Contains("left") || buttonmode.Contains("right") || buttonmode.Contains("off"))
                {
                    if (clearmode.Equals(buttonmode))
                    {
                        return $"resource://AppIoT.Resources.SVG.{buttonmode}-on.svg";
                    }
                    else
                    {
                        return $"resource://AppIoT.Resources.SVG.{buttonmode}-off.svg";
                    }
                }
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
