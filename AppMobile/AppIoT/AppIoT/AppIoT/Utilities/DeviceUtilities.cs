﻿using AppIoT.Helpers;
using AppIoT.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AppIoT.Utilities
{
    public static class DeviceUtilities
    {
        public async static Task<bool> UpdateWifi(UpdateWifiDeviceModel updateWifiDeviceModel, Action<string> callback)
        {
            if (string.IsNullOrEmpty(updateWifiDeviceModel.SSID))
            {
                callback("SSID không được trống.");
                return false;
            }
            if (string.IsNullOrEmpty(updateWifiDeviceModel.Password))
            {
                callback("Mật khẩu không được trống.");
                return false;
            }
            if (string.IsNullOrEmpty(updateWifiDeviceModel.DeviceId))
            {
                callback("DeviceId không được trống.");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                try
                {
                    var response = await http.GetAsync($"http://192.168.4.1?mywifi={updateWifiDeviceModel.SSID}&pass={updateWifiDeviceModel.Password}&user_id={constants.Account.Id}&device_id={updateWifiDeviceModel.DeviceId}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();                        
                        return responeContent.Equals("OK") ? true : false;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        callback(responeContent);
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    callback(ee.Message);
                }
            }

            return false;
        }
    }
}
