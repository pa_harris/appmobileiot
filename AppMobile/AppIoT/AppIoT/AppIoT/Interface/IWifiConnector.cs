﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppIoT.Interface
{
    public interface IWifiConnector
    {
        void Connect(string ssid, string password);
        string CurrentSSID();
        void OpenWifiSetting();
    }
}
