﻿using AppIoT.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppIoT.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OTPConfirmationPage : ContentPage
    {
        public OTPConfirmationPageViewModel ViewModel { get; set; }
        public OTPConfirmationPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new OTPConfirmationPageViewModel(this);
        }
    }
}