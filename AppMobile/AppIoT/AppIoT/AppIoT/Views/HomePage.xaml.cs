﻿using AppIoT.Helpers;
using AppIoT.Models;
using AppIoT.Services;
using AppIoT.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppIoT.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        public HomePageViewModel ViewModel { get; set; }
        public HomePage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new HomePageViewModel(this);
        }

        //private void lsvItemTapped(object sender, ItemTappedEventArgs e)
        //{
        //    var dataContext = e.Item as DeviceInfo;
        //    string deviceId = dataContext.DeviceId;
        //    ViewModel.IsBusy = true;
        //    ViewModel.LoadPage(new DeviceControlPage(deviceId));
        //}
    }
}