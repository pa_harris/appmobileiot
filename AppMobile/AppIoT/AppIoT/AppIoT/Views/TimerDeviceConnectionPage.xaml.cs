﻿using AppIoT.Models;
using AppIoT.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppIoT.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TimerDeviceConnectionPage : ContentPage
	{
        public TimerDeviceConnectionPageViewModel ViewModel { get; set; }
        public TimerDeviceConnectionPage (UpdateWifiDeviceModel updateWifiDeviceModel)
		{
			InitializeComponent ();
			BindingContext = ViewModel = new TimerDeviceConnectionPageViewModel(this, updateWifiDeviceModel);
		}
	}
}