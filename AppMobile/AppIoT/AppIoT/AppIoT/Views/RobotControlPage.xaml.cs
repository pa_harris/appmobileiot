﻿using AppIoT.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppIoT.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RobotControlPage : ContentPage
	{
        public RobotControlPageViewModel ViewModel { get; set; }
        public RobotControlPage ()
		{
			InitializeComponent();
			BindingContext = ViewModel = new RobotControlPageViewModel(this);
		}
	}
}