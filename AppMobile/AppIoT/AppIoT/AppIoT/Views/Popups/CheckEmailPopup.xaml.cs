﻿using AppIoT.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppIoT.Views.Popups
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CheckEmailPopup : PopupPage
    {
        public CheckEmailPopup(ForgotPasswordPageViewModel parentViewModel)
        {
            InitializeComponent();
            if (parentViewModel != null)
            {
                BindingContext = parentViewModel;
            }
            else
            {
                BindingContext = new ForgotPasswordPageViewModel(null, null);
            }
        }
    }
}