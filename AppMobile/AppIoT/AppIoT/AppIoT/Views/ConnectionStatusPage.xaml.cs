﻿using AppIoT.Models;
using AppIoT.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppIoT.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ConnectionStatusPage : ContentPage
	{
        public ConnectionStatusPageViewModel ViewModel { get; set; }
        public ConnectionStatusPage (EDeviceType deviceType, bool isConnected = true)
		{
			InitializeComponent ();
			BindingContext = ViewModel = new ConnectionStatusPageViewModel(this, deviceType, isConnected);
		}
	}
}