﻿using AppIoT.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppIoT.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DeviceControlPage : ContentPage
	{
        public DeviceControlPageViewModel ViewModel { get; set; }
        public DeviceControlPage(string deviceId)
		{
			InitializeComponent();
			BindingContext = ViewModel = new DeviceControlPageViewModel(this, deviceId);
		}
	}
}