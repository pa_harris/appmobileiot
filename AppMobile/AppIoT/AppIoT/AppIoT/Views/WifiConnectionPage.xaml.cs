﻿using AppIoT.Models;
using AppIoT.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppIoT.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WifiConnectionPage : ContentPage
	{
        public WifiConnectionPageViewModel ViewModel { get; set; }
        public WifiConnectionPage (EDeviceType deviceType = EDeviceType.Switch)
		{
			InitializeComponent ();
			BindingContext = ViewModel = new WifiConnectionPageViewModel(this, deviceType);
		}
	}
}