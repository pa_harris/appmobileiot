﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppIoT.UCs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OTPView : ContentView
    {
        #region Properties
        private string _OTP1;
        public string OTP1 { get => _OTP1; set { _OTP1 = value; OnPropertyChanged(); UpdateOTP(); } }
        private string _OTP2;
        public string OTP2 { get => _OTP2; set { _OTP2 = value; OnPropertyChanged(); UpdateOTP(); } }
        private string _OTP3;
        public string OTP3 { get => _OTP3; set { _OTP3 = value; OnPropertyChanged(); UpdateOTP(); } }
        private string _OTP4;
        public string OTP4 { get => _OTP4; set { _OTP4 = value; OnPropertyChanged(); UpdateOTP(); } }
        public string OTP
        {
            get { return (string)base.GetValue(OTPProperty); }
            set { base.SetValue(OTPProperty, value); }
        }
        public static readonly BindableProperty OTPProperty = BindableProperty.Create(
                                                         propertyName: "OTP",
                                                         returnType: typeof(string),
                                                         declaringType: typeof(OTPView),
                                                         defaultValue: "",
                                                         defaultBindingMode: BindingMode.TwoWay);

        #endregion
        public OTPView()
        {
            InitializeComponent();
            BindingContext = this;
            Entry entry1 = FindByName("entry1") as Entry;
            entry1.Focus();
        }

        private void UpdateOTP()
        {
            if (!string.IsNullOrEmpty(OTP1) && !string.IsNullOrEmpty(OTP2) && !string.IsNullOrEmpty(OTP3) && !string.IsNullOrEmpty(OTP4))
            {
                OTP = OTP1 + OTP2 + OTP3 + OTP4;
            }
            else
            {
                OTP = "";
            }
        }

        private void entry1_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry entry2 = FindByName("entry2") as Entry;
            if (e.NewTextValue != "")
            {
                entry2.Focus();
            }
        }
        private void entry2_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry entry1 = FindByName("entry1") as Entry;
            Entry entry3 = FindByName("entry3") as Entry;
            if (e.NewTextValue != "")
            {
                entry3.Focus();
            }
            else
            {
                entry1.Focus();
            }
        }
        private void entry3_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry entry4 = FindByName("entry4") as Entry;
            Entry entry2 = FindByName("entry2") as Entry;
            if (e.NewTextValue != "")
            {
                entry4.Focus();
            }
            else
            {
                entry2.Focus();
            }
        }
        private void entry4_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry entry3 = FindByName("entry3") as Entry;
            if (e.NewTextValue != "")
            {
                // do nothing
            }
            else
            {
                entry3.Focus();
            }
        }

        private void entry_Focused(object sender, FocusEventArgs e)
        {
            var entry = sender as EntryWithoutUnderline;
            entry.TextColor = Color.FromHex("#1F53E9");
            var frameParent = entry.Parent as Frame;
            frameParent.BorderColor = Color.FromHex("#1F53E9");
        }
        private void entry_Unfocused(object sender, FocusEventArgs e)
        {
            var entry = sender as EntryWithoutUnderline;
            entry.TextColor = Color.Black;
            var frameParent = entry.Parent as Frame;
            frameParent.BorderColor = Color.FromHex("#D9D6D6");
        }

    }
}