﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppIoT.UCs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntryWithIcon : ContentView
    {
        #region Properties
        private Color _Border;
        public Color Border { get => _Border; set { _Border = value; OnPropertyChanged(); } }
        public string Placeholder
        {
            get { return base.GetValue(PlaceholderProperty).ToString(); }
            set { base.SetValue(PlaceholderProperty, value); }
        }
        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create(
                                                         propertyName: "Placeholder",
                                                         returnType: typeof(string),
                                                         declaringType: typeof(EntryWithIcon),
                                                         defaultValue: "",
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: PlaceholderPropertyChanged);
        private static void PlaceholderPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue != oldValue && newValue != null)
            {
                var control = (EntryWithIcon)bindable;
                control.entry.Placeholder = newValue.ToString();
            }
        }

        public string FirstIcon
        {
            get { return base.GetValue(FirstIconProperty).ToString(); }
            set { base.SetValue(FirstIconProperty, value); }
        }
        public static readonly BindableProperty FirstIconProperty = BindableProperty.Create(
                                                         propertyName: "FirstIcon",
                                                         returnType: typeof(string),
                                                         declaringType: typeof(EntryWithIcon),
                                                         defaultValue: "",
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: FirstIconPropertyChanged);
        private static void FirstIconPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue != oldValue && newValue != null)
            {
                var control = (EntryWithIcon)bindable;
                control.startIcon.Source = newValue.ToString();
                control.startIcon.IsVisible = true;
            }
        }
        public string SecondIcon
        {
            get { return base.GetValue(SecondIconProperty).ToString(); }
            set { base.SetValue(SecondIconProperty, value); }
        }
        public static readonly BindableProperty SecondIconProperty = BindableProperty.Create(
                                                         propertyName: "SecondIcon",
                                                         returnType: typeof(string),
                                                         declaringType: typeof(EntryWithIcon),
                                                         defaultValue: "",
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: SecondIconPropertyChanged);
        private static void SecondIconPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue.ToString().Contains("eye") || newValue.ToString().Contains("down"))
            {
                var control = (EntryWithIcon)bindable;
                control.stackEndIcon.IsEnabled = true;
                control.stackEndIcon.IsVisible = true;
                if (newValue != oldValue && newValue != null)
                {
                    control.endIcon.Source = newValue.ToString();
                    control.endIcon.IsVisible = true;
                }
            }
            else
            {
                var control = (EntryWithIcon)bindable;
                control.stackEndIcon.IsEnabled = false;
                control.stackEndIcon.IsVisible = false;
            }
        }

        public string TextEntry
        {
            get { return base.GetValue(TextEntryProperty).ToString(); }
            set { base.SetValue(TextEntryProperty, value); }
        }
        public static readonly BindableProperty TextEntryProperty = BindableProperty.Create(
                                                         propertyName: "TextEntry",
                                                         returnType: typeof(string),
                                                         declaringType: typeof(EntryWithIcon),
                                                         defaultValue: "",
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: TextEntryPropertyChanged);
        private static void TextEntryPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue != oldValue && newValue != null)
            {
                var control = (EntryWithIcon)bindable;
                control.entry.Text = newValue.ToString();
            }
        }
        #endregion
        public EntryWithIcon()
        {
            InitializeComponent();
            BindingContext = this;
            Border = Color.FromHex("#D9D6D6");
        }
        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            StackLayout stack = sender as StackLayout;
            if (stack != null && e != null)
            {
                if (SecondIcon.Contains("eye"))
                {
                    SecondIcon = (SecondIcon.Contains("eye-close")) ? "resource://AppIoT.Resources.SVG.eye-open.svg" : "resource://AppIoT.Resources.SVG.eye-close.svg";
                }
            }
        }

        private void entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            EntryWithoutUnderline entSender = sender as EntryWithoutUnderline;
            if (entSender != null && e != null)
            {
                if (e.NewTextValue != e.OldTextValue)
                {
                    TextEntry = e.NewTextValue;
                }
            }
        }
        private void entry_Focused(object sender, FocusEventArgs e)
        {
            Border = Color.FromHex("#1F53E9");
        }
        private void entry_Unfocused(object sender, FocusEventArgs e)
        {
            Border = Color.FromHex("#D9D6D6");
        }
    }
}