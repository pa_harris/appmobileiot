﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppIoT.Models
{
    public static class AddNewDeviceModel
    {
        public static string DeviceId { get; set; }
        public static string DeviceName { get; set; }
        public static EDeviceType DeviceType { get; set; }

    }
}
