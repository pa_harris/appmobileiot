﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppIoT.Models
{
    public class UpdateWifiDeviceModel
    {
        public string SSID { get; set; }
        public string Password { get; set; }
        public string DeviceId { get; set; }
    }
}
