﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppIoT.Models
{
    public class DeviceModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public EDeviceType Type { get; set; }
        public EDeviceStatus Status { get; set; }
    }
}
