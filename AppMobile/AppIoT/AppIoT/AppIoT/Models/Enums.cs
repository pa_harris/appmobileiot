﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppIoT.Models
{
    public enum EOTPActionType : byte
    {
        AccountRegister = 1,
        AccountForgotPassword
    }
    public enum LoginProvider
    {
        Facebook = 1,
        Google,
        Apple
    }
    public enum EDeviceType
    {
        Socket = 1,
        Switch,
        Robot
    }
    public enum EDeviceStatus
    {
        On = 1,
        Off,
        Disconnected
    }
}
