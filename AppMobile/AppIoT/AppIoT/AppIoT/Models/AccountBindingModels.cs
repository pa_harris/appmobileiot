﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppIoT.Models
{
    public class AccountLoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class ForgotPasswordBindingModel
    {
        public string Email { get; set; }
    }
    public class ResetPasswordBindingModel
    {
        public string Email { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string OTP { get; set; }
    }
    public class ChangePasswordBindingModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
    public class ResendOtpModel
    {
        public string Email { get; set; }
        public EOTPActionType ActionType { get; set; }
    }
    public class ConfirmOtpModel
    {
        public string Email { get; set; }
        public string Fullname { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Otp { get; set; }
    }
    public class RegisterBindingModel
    {
        public string Email { get; set; }
    }
    public class ExternalLoginBindingModel
    {
        public string LoginProvider { get; set; }
        public string ExternalAccessToken { get; set; }
    }
}
