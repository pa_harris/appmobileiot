﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppIoT.Models
{
    public class UserInfoViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public string SharingId { get; set; }
    }
    public class LoginResponseSucceed
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string user_name { get; set; }
        public string issued { get; set; }
        public string expires { get; set; }
    }
    public class LoginResponseFailed
    {
        public string error { get; set; }
        public string error_description { get; set; }
    }
    public class MessageFailed
    {
        public string Message { get; set; }
    }
}
