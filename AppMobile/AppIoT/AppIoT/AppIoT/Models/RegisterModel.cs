﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppIoT.Models
{
    public static class RegisterModel
    {
        public static string Email { get; set; }
        public static string Password { get; set; }
        public static string ConfirmPassword { get; set; }
        public static string OTP { get; set; }
    }
}
