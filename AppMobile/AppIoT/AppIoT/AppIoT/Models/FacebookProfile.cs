﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AppIoT.Models
{
	public class FacebookProfile
	{
		public string FullName { get; set; }
		public string Email { get; set; }
		public string Token { get; set; }
		public UriImageSource Picture { get; set; }
	}
}
