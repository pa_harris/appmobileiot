﻿using AppIoT.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace AppIoT.Models
{
    public class DeviceInfo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private string _DeviceId;
        public string DeviceId 
        {
            get => _DeviceId;
            set
            {
                _DeviceId = value;
                Type = value.Contains("socket") ? EDeviceType.Socket : value.Contains("switch") ? EDeviceType.Switch : EDeviceType.Robot;
                OnPropertyChanged(nameof(Type));
            }
        }
        private long _timestamp = 0;// (long)DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        public long timestamp
        {
            get => _timestamp;
            set
            {
                _timestamp = value;
                if (DateTime.Now.ToUniversalTime().Subtract(DateTimeHelper.UnixTimeStampToDateTime(value)).TotalSeconds > 60)
                {
                    EStatus = EDeviceStatus.Off;
                }
                else
                {
                    EStatus = EDeviceStatus.On;
                    OnPropertyChanged(nameof(EStatus));
                    int CurrentTimer = 60;
                    Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                    {
                        if (CurrentTimer > 0)
                        {
                            CurrentTimer--;
                            return true;
                        }
                        EStatus = EDeviceStatus.Off;
                        OnPropertyChanged(nameof(EStatus));
                        return false;
                    });
                }
            }
        }
        public string name { get; set; }
        public string status { get; set; } = "off";
        public EDeviceStatus EStatus { get; set; } = EDeviceStatus.Disconnected;
        public EDeviceType Type { get; set; }
    }
    public class RobotInfo : DeviceInfo
    {
        public RobotInfo(DeviceInfo deviceInfo)
        {
            this.DeviceId = deviceInfo.DeviceId;
            this.timestamp = deviceInfo.timestamp;
            this.name = deviceInfo.name;
            this.status = deviceInfo.status;
        }
        public RobotInfo()
        { }
        public string Tramsac { get; set; } = "off";
        public string clear { get; set; } = "null";
        public string control { get; set; } = "null";
        public string fanmode { get; set; } = "on";
        public string pin { get; set; } = "80%";
        public string stop { get; set; } = "on";
    }
}
