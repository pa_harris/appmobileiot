﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using AppIoTWebAPI.Models;
using AppIoTWebAPI.Providers;
using AppIoTWebAPI.Results;
using System.Web.Http.Description;
using System.Linq;
using AppIoTWebAPI.Helper;
using AppIoTWebAPI.Data;
using Newtonsoft.Json;
using System.Configuration;

namespace AppIoTWebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private AppIOTEntities db = new AppIOTEntities();
        private string facebookAppId = ConfigurationManager.AppSettings["FacebookAppId"];
        private string siteAuthor = ConfigurationManager.AppSettings["SiteAuthor"];

        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        [ResponseType(typeof(UserInfoViewModel))]
        public IHttpActionResult GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            var user = UserManager.FindById(User.Identity.GetUserId());
            var userInfo = db.UserInfoes.Find(User.Identity.GetUserId());

            if (user == null || userInfo == null)
            {
                ModelState.AddModelError("user_not_found", "Người dùng không tồn tại.");
                return BadRequest(ModelState);

            }

            var result = new UserInfoViewModel
            {
                Id = User.Identity.GetUserId(),
                Email = User.Identity.GetUserName(),
                Avatar = userInfo.Avatar,
                SharingId = userInfo.SharingId
            };

            return Ok(result);
        }

        // POST api/Account/ResendOtp
        [AllowAnonymous]
        [ResponseType(typeof(bool))]
        [Route("ResendOtp")]
        public IHttpActionResult ResendOtp(ResendOtpModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApplicationUser user = UserManager.FindByEmail(model.Email);
            string target = model.Email;

            if (user != null)
            {
                var otp = db.OTPs.FirstOrDefault(x => x.Target == target && x.ActionType == (byte)model.ActionType);
                if (otp == null)
                {
                    otp = new OTP();
                    otp.Code = Utilities.GenerateRandomOTP();
                    otp.CreateTime = DateTime.Now;
                    otp.ExpirationTime = otp.CreateTime.AddMinutes(5);
                    otp.Target = target;
                    otp.ActionType = (byte)model.ActionType;
                    db.OTPs.Add(otp);
                }
                else
                {
                    if (otp.ExpirationTime < DateTime.Now)
                    {
                        otp.Code = Utilities.GenerateRandomOTP();
                        otp.CreateTime = DateTime.Now;
                        otp.ExpirationTime = otp.CreateTime.AddMinutes(5);
                        otp.Target = target;
                        otp.ActionType = (byte)model.ActionType;
                    }
                }
                db.SaveChanges();

                #region send mail
                // send mail
                var body = EmailHelper.RenderOtpEmail(model.Email, otp.Code, model.ActionType);

                Task.Run(() =>
                {
                    EmailService sendmail = new EmailService();
                    sendmail.SendAsync(new IdentityMessage
                    {
                        Destination = user.Email,
                        Subject = "Akashiccorp OTP verify/ Mã xác thực OTP",
                        Body = body
                    });
                });
                #endregion

                return Ok(true);
            }

            return Ok(false);
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var otpTarget = model.Email;
                ApplicationUser user = await UserManager.FindByEmailAsync(otpTarget);

                if (user != null)
                {
                    ModelState.AddModelError("user_existed", "Tài khoản đã tồn tại!");
                    return BadRequest(ModelState);
                }

                var otp = db.OTPs.FirstOrDefault(x => x.Target == otpTarget && x.ActionType == (byte)EOTPActionType.AccountRegister);
                if (otp == null)
                {
                    otp = new OTP();
                    otp.Code = Utilities.GenerateRandomOTP();
                    otp.CreateTime = DateTime.Now;
                    otp.ExpirationTime = otp.CreateTime.AddMinutes(5);
                    otp.Target = otpTarget;
                    otp.ActionType = (byte)EOTPActionType.AccountRegister;
                    db.OTPs.Add(otp);
                }
                else
                {
                    if (otp.ExpirationTime < DateTime.Now)
                    {
                        otp.Code = Utilities.GenerateRandomOTP();
                        otp.CreateTime = DateTime.Now;
                        otp.ExpirationTime = otp.CreateTime.AddMinutes(5);
                        otp.Target = otpTarget;
                        otp.ActionType = (byte)EOTPActionType.AccountRegister;
                    }
                }

                await db.SaveChangesAsync();

                #region send mail
                // send mail
                var body = EmailHelper.RenderOtpEmail(model.Email, otp.Code, EOTPActionType.AccountRegister);

                await Task.Run(() =>
                {
                    EmailService sendmail = new EmailService();
                    sendmail.SendAsync(new IdentityMessage
                    {
                        Destination = model.Email,
                        Subject = "Akashiccorp OTP verify/ Mã xác thực OTP",
                        Body = body
                    });
                });
                #endregion

                return Ok(true);
            }
            catch (Exception e)
            {
                return Ok(e.InnerException);

            }
            
        }

        [AllowAnonymous]
        [Route("ExternalLogins")]
        [ResponseType(typeof(ExternalLoginRespone))]
        public async Task<IHttpActionResult> ExternalLogins(ExternalLoginBindingModel model)
        {
            var externalLogin = new ExternalLoginBindingModel();
            ExternalCreateAccountModel dataCreateAccount = new ExternalCreateAccountModel();

            if (model.LoginProvider.Equals("Facebook"))
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {model.ExternalAccessToken}");

                using (var response = await client.GetAsync("https://graph.facebook.com/app"))
                {
                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                        {
                            ModelState.AddModelError("facebook_unauthorized", "Ứng dụng Facebook không được cấp quyền!");
                        }
                        return BadRequest(ModelState);
                    }

                    var responseString = await response.Content.ReadAsStringAsync();
                    var dataAppInfo = JsonConvert.DeserializeObject<FacebookResponseAppInfo>(responseString);
                    if (dataAppInfo.Id != facebookAppId)
                    {
                        return BadRequest();
                    }
                }

                using (var response = await client.GetAsync("https://graph.facebook.com/v5.0/me?fields=name,email,id"))
                {
                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        return BadRequest();
                    }

                    var responseString = await response.Content.ReadAsStringAsync();
                    var dataUserInfo = JsonConvert.DeserializeObject<FacebookResponseUserInfo>(responseString);

                    dataCreateAccount.Email = dataUserInfo.Email;
                    dataCreateAccount.Id = dataUserInfo.Id;
                    dataCreateAccount.Fullname = dataUserInfo.Name;
                }
            }
            else if (model.LoginProvider.Equals("Google"))
            {
                var client = new HttpClient();
                using (var response = await client.GetAsync($"https://www.googleapis.com/oauth2/v1/userinfo?access_token={model.ExternalAccessToken}"))
                {
                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        return BadRequest();
                    }

                    var responseString = await response.Content.ReadAsStringAsync();
                    var dataUserInfo = JsonConvert.DeserializeObject<GoogleUserInfoResponse>(responseString);

                    dataCreateAccount.Email = dataUserInfo.Email;
                    dataCreateAccount.Id = dataUserInfo.Id;
                    dataCreateAccount.Fullname = dataUserInfo.Name;
                }
            }

            #region CreateAccount
            string providerKey = string.Empty;
            if (string.IsNullOrEmpty(dataCreateAccount.Email))
            {
                ModelState.AddModelError("email_cannot_be_empty", "Email không được trống!");
                return BadRequest(ModelState);
            }

            providerKey = dataCreateAccount?.Id;

            if (!string.IsNullOrEmpty(providerKey))
            {
                ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(model.LoginProvider, providerKey));

                if (user == null)
                {
                    user = new ApplicationUser() { UserName = dataCreateAccount.Email, Email = dataCreateAccount.Email, EmailConfirmed = true };

                    var result = await UserManager.CreateAsync(user);
                    if (result.Succeeded)
                    {
                        result = await UserManager.AddLoginAsync(user.Id, new UserLoginInfo(model.LoginProvider, providerKey));
                        if (result.Succeeded)
                        {
                            UserInfo userInfo = new UserInfo();
                            userInfo.UserId = user.Id;
                            userInfo.Avatar = "/Content/img/no-user.png";
                            userInfo.SharingId = DateTime.Now.ToFileTime().ToString();
                            db.UserInfoes.Add(userInfo);
                            await db.SaveChangesAsync();

                            #region send mail
                            // send mail
                            var body = EmailHelper.RenderWelcomeEmail(dataCreateAccount.Email);

                            await Task.Run(() =>
                            {
                                EmailService sendmail = new EmailService();
                                sendmail.SendAsync(new IdentityMessage
                                {
                                    Destination = dataCreateAccount.Email,
                                    Subject = "Đăng ký tài khoản " + siteAuthor,
                                    Body = body
                                });
                            });
                            #endregion
                        }
                    }
                    else
                    {
                        return GetErrorResult(result);
                    }
                }

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);
                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName, user.SecurityStamp);
                return Ok(new ExternalLoginRespone()
                {
                    AccessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(new AuthenticationTicket(oAuthIdentity, properties)),
                    TokenType = "bearer",
                    UserName = user.UserName
                });
            }

            return BadRequest();
            #endregion
        }

        // POST api/Account/RegisterConfirmOtp
        [AllowAnonymous]
        [ResponseType(typeof(bool))]
        [Route("RegisterConfirmOtp")]
        public async Task<IHttpActionResult> RegisterConfirmOtp(ConfirmOtpModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var otpTarget = model.Email;

            var otp = db.OTPs.FirstOrDefault(x => x.Target == otpTarget && x.ActionType == (byte)EOTPActionType.AccountRegister);
            
            if (otp != null)
            {
                if (otp.ExpirationTime <= DateTime.Now)
                {
                    ModelState.AddModelError("otp_expired", "OTP đã hết hạn.");
                    return BadRequest(ModelState);
                }
                if (otp.Code != model.Otp)
                {
                    ModelState.AddModelError("wrong_otp", "OTP không đúng.");
                    return BadRequest(ModelState);
                }
                ApplicationUser user = new ApplicationUser() { UserName = model.Email, Email = model.Email, EmailConfirmed = true };
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }

                UserInfo userInfo = new UserInfo();
                userInfo.UserId = user.Id;
                userInfo.Avatar = "/Content/img/no-user.png";
                userInfo.SharingId = DateTime.Now.ToFileTime().ToString();
                db.UserInfoes.Add(userInfo);
                await db.SaveChangesAsync();

                #region send mail
                // send mail
                var body = EmailHelper.RenderWelcomeEmail(model.Email);

                await Task.Run(() =>
                {
                    EmailService sendmail = new EmailService();
                    sendmail.SendAsync(new IdentityMessage
                    {
                        Destination = model.Email,
                        Subject = "Đăng ký tài khoản " + siteAuthor,
                        Body = body
                    });
                });
                #endregion
                return Ok(true);
            }
            ModelState.AddModelError("otp_not_sent", "OTP chưa được gửi.");
            return BadRequest(ModelState);
        }

        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/ForgotPassword
        [AllowAnonymous]
        [ResponseType(typeof(bool))]
        [Route("ForgotPassword")]
        public IHttpActionResult ForgotPassword(ForgotPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApplicationUser user = UserManager.FindByEmail(model.Email);
            string target = model.Email;

            if (user != null)
            {
                var otp = db.OTPs.FirstOrDefault(x => x.Target == target && x.ActionType == (byte)EOTPActionType.AccountForgotPassword);
                if (otp == null)
                {
                    otp = new OTP();
                    otp.Code = Utilities.GenerateRandomOTP();
                    otp.CreateTime = DateTime.Now;
                    otp.ExpirationTime = otp.CreateTime.AddMinutes(5);
                    otp.Target = target;
                    otp.ActionType = (byte)EOTPActionType.AccountForgotPassword;
                    db.OTPs.Add(otp);
                }
                else
                {
                    if (otp.ExpirationTime < DateTime.Now)
                    {
                        otp.Code = Utilities.GenerateRandomOTP();
                        otp.CreateTime = DateTime.Now;
                        otp.ExpirationTime = otp.CreateTime.AddMinutes(5);
                        otp.Target = target;
                        otp.ActionType = (byte)EOTPActionType.AccountForgotPassword;
                    }
                }
                db.SaveChanges();

                #region send mail
                // send mail
                var body = EmailHelper.RenderOtpEmail(model.Email, otp.Code, EOTPActionType.AccountForgotPassword);

                Task.Run(() =>
                {
                    EmailService sendmail = new EmailService();
                    sendmail.SendAsync(new IdentityMessage
                    {
                        Destination = user.Email,
                        Subject = "Password Reset/ Khôi phục mật khẩu",
                        Body = body
                    });
                });
                #endregion

                return Ok(true);
            }
            ModelState.AddModelError("user_not_found", "Không tìm thấy người dùng.");
            return BadRequest(ModelState);
        }

        // POST api/Account/ResetPassword
        [AllowAnonymous]
        [ResponseType(typeof(bool))]
        [Route("ResetPassword")]
        public IHttpActionResult ResetPassword(ResetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApplicationUser user = UserManager.FindByEmail(model.Email);
            var otpTarget = model.Email;

            if (user != null)
            {
                var otp = db.OTPs.FirstOrDefault(x => x.Target == otpTarget && x.ActionType == (byte)EOTPActionType.AccountForgotPassword);
                if (otp != null)
                {
                    if (otp.ExpirationTime <= DateTime.Now)
                    {
                        ModelState.AddModelError("otp_expired", "OTP đã hết hạn.");
                        return BadRequest(ModelState);
                    }
                    if (otp.Code != model.OTP)
                    {
                        ModelState.AddModelError("wrong_otp", "OTP không đúng.");
                        return BadRequest(ModelState);
                    }
                    db.OTPs.Remove(otp);
                    db.SaveChanges();

                    user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.NewPassword);
                    UserManager.Update(user);

                    UserManager.UpdateSecurityStamp(user.Id);
                    return Ok(true);
                }
            }
            ModelState.AddModelError("user_not_found", "Không tìm thấy người dùng.");
            return BadRequest(ModelState);
        }

        //// POST api/Account/SetPassword
        //[Route("SetPassword")]
        //public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        //[AllowAnonymous]
        //[Route("ExternalLogins")]
        //public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        //{
        //    IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
        //    List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

        //    string state;

        //    if (generateState)
        //    {
        //        const int strengthInBits = 256;
        //        state = RandomOAuthStateGenerator.Generate(strengthInBits);
        //    }
        //    else
        //    {
        //        state = null;
        //    }

        //    foreach (AuthenticationDescription description in descriptions)
        //    {
        //        ExternalLoginViewModel login = new ExternalLoginViewModel
        //        {
        //            Name = description.Caption,
        //            Url = Url.Route("ExternalLogin", new
        //            {
        //                provider = description.AuthenticationType,
        //                response_type = "token",
        //                client_id = Startup.PublicClientId,
        //                redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
        //                state = state
        //            }),
        //            State = state
        //        };
        //        logins.Add(login);
        //    }

        //    return logins;
        //}

        

        //// POST api/Account/Logout
        //[Route("Logout")]
        //public IHttpActionResult Logout()
        //{
        //    Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
        //    return Ok();
        //}

        //// GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        //[Route("ManageInfo")]
        //public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        //{
        //    IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

        //    if (user == null)
        //    {
        //        return null;
        //    }

        //    List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

        //    foreach (IdentityUserLogin linkedAccount in user.Logins)
        //    {
        //        logins.Add(new UserLoginInfoViewModel
        //        {
        //            LoginProvider = linkedAccount.LoginProvider,
        //            ProviderKey = linkedAccount.ProviderKey
        //        });
        //    }

        //    if (user.PasswordHash != null)
        //    {
        //        logins.Add(new UserLoginInfoViewModel
        //        {
        //            LoginProvider = LocalLoginProvider,
        //            ProviderKey = user.UserName,
        //        });
        //    }

        //    return new ManageInfoViewModel
        //    {
        //        LocalLoginProvider = LocalLoginProvider,
        //        Email = user.UserName,
        //        Logins = logins,
        //        ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
        //    };
        //}

        //// POST api/Account/ChangePassword
        
        //// POST api/Account/AddExternalLogin
        //[Route("AddExternalLogin")]
        //public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

        //    AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

        //    if (ticket == null || ticket.Identity == null || (ticket.Properties != null
        //        && ticket.Properties.ExpiresUtc.HasValue
        //        && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
        //    {
        //        return BadRequest("External login failure.");
        //    }

        //    ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

        //    if (externalData == null)
        //    {
        //        return BadRequest("The external login is already associated with an account.");
        //    }

        //    IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
        //        new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// POST api/Account/RemoveLogin
        //[Route("RemoveLogin")]
        //public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    IdentityResult result;

        //    if (model.LoginProvider == LocalLoginProvider)
        //    {
        //        result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
        //    }
        //    else
        //    {
        //        result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
        //            new UserLoginInfo(model.LoginProvider, model.ProviderKey));
        //    }

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// GET api/Account/ExternalLogin
        //[OverrideAuthentication]
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        //[AllowAnonymous]
        //[Route("ExternalLogin", Name = "ExternalLogin")]
        //public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        //{
        //    if (error != null)
        //    {
        //        return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
        //    }

        //    if (!User.Identity.IsAuthenticated)
        //    {
        //        return new ChallengeResult(provider, this);
        //    }

        //    ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

        //    if (externalLogin == null)
        //    {
        //        return InternalServerError();
        //    }

        //    if (externalLogin.LoginProvider != provider)
        //    {
        //        Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
        //        return new ChallengeResult(provider, this);
        //    }

        //    ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
        //        externalLogin.ProviderKey));

        //    bool hasRegistered = user != null;

        //    if (hasRegistered)
        //    {
        //        Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                
        //         ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
        //            OAuthDefaults.AuthenticationType);
        //        ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
        //            CookieAuthenticationDefaults.AuthenticationType);

        //        AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
        //        Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
        //    }
        //    else
        //    {
        //        IEnumerable<Claim> claims = externalLogin.GetClaims();
        //        ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
        //        Authentication.SignIn(identity);
        //    }

        //    return Ok();
        //}

        //// POST api/Account/RegisterExternal
        //[OverrideAuthentication]
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        //[Route("RegisterExternal")]
        //public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var info = await Authentication.GetExternalLoginInfoAsync();
        //    if (info == null)
        //    {
        //        return InternalServerError();
        //    }

        //    var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

        //    IdentityResult result = await UserManager.CreateAsync(user);
        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    result = await UserManager.AddLoginAsync(user.Id, info.Login);
        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result); 
        //    }
        //    return Ok();
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
