﻿using System;
using System.ComponentModel.DataAnnotations;
using AppIoTWebAPI.Data;
using Newtonsoft.Json;

namespace AppIoTWebAPI.Models
{
    // Models used as parameters to AccountController actions.

    public class ResendOtpModel
    {
        [EmailAddress(ErrorMessage = "Email không đúng định dạng.")]
        [MaxLength(256)]
        [Required(ErrorMessage = "Email không được trống.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "ActionType không được trống.")]
        public EOTPActionType ActionType { get; set; }
    }

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        [Required(ErrorMessage = "Mật khẩu hiện tại không được trống.")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Mật khẩu mới không được trống.")]
        [StringLength(100, ErrorMessage = "Mật khẩu chứa ít nhất {2} kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Mật khẩu mới và xác nhận mật khẩu không khớp.")]
        public string ConfirmPassword { get; set; }
    }
    public class ForgotPasswordBindingModel
    {
        [EmailAddress]
        [MaxLength(256)]
        [Required(ErrorMessage = "Email không được trống.")]
        public string Email { get; set; }
    }

    public class ResetPasswordBindingModel
    {
        [EmailAddress]
        [MaxLength(256)]
        [Required(ErrorMessage = "Email không được trống")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mật khẩu mới không được trống")]
        [StringLength(100, ErrorMessage = "Mật khẩu chứa ít nhất {2} kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Mật khẩu mới và xác nhận mật khẩu không khớp.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "OTP không được trống")]
        [StringLength(4, ErrorMessage = "OTP chứa ít nhất {2} kí tự.", MinimumLength = 4)]
        public string OTP { get; set; }
    }


    public class RegisterBindingModel
    {
        [EmailAddress]
        [Required(ErrorMessage = "Email không được trống.")]
        [MaxLength(256)]
        public string Email { get; set; }
    }

    public class ExternalLoginBindingModel
    {
        [Required(ErrorMessage = "Login provider không được trống")]
        public string LoginProvider { get; set; }

        [Required(ErrorMessage = "External access token không được trống")]
        public string ExternalAccessToken { get; set; }
    }

    public class ExternalCreateAccountModel
    {
        public string Email { get; set; }
        public string Fullname { get; set; }
        public string Id { get; set; }
    }

    public class FacebookResponseAppInfo
    {
        public string Id { get; set; }
    }

    public class FacebookResponseUserInfo
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
    }

    public class GoogleUserInfoResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }

    public class ConfirmOtpModel
    {
        [EmailAddress]
        [Required(ErrorMessage = "Email không được trống")]
        [MaxLength(256)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mật khẩu không được trống")]
        [StringLength(100, ErrorMessage = "Mật khẩu chứa ít nhất {2} kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Mật khẩu mới và xác nhận mật khẩu không khớp.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "OTP không được trống")]
        [StringLength(4, ErrorMessage = "OTP chứa {2} kí tự.", MinimumLength = 4)]
        public string Otp { get; set; }
    }

    //public class RegisterExternalBindingModel
    //{
    //    [Required]
    //    [Display(Name = "Email")]
    //    public string Email { get; set; }
    //}

    //public class RemoveLoginBindingModel
    //{
    //    [Required]
    //    [Display(Name = "Login provider")]
    //    public string LoginProvider { get; set; }

    //    [Required]
    //    [Display(Name = "Provider key")]
    //    public string ProviderKey { get; set; }
    //}

    //public class SetPasswordBindingModel
    //{
    //    [Required]
    //    [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
    //    [DataType(DataType.Password)]
    //    [Display(Name = "New password")]
    //    public string NewPassword { get; set; }

    //    [DataType(DataType.Password)]
    //    [Display(Name = "Confirm new password")]
    //    [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
    //    public string ConfirmPassword { get; set; }
    //}
}
