﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppIoTWebAPI.Helper
{
    public static class Utilities
    {
        public static string GenerateRandomOTP()
        {
            Random rdm = new Random();
            return rdm.Next(0, 9999).ToString("D4");
        }
    }
}
