﻿using AppIoTWebAPI.Data;
using Microsoft.AspNet.Identity;
using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AppIoTWebAPI.Helper
{
    public static class EmailHelper
    {
        public static string RenderForgotPasswordEmail(string otp)
        {
            var body = "<p style=\" font-size: 1.8vw; margin-left: 15vw; margin-top: 5vw;\" >Hello!</p>" +
                        "<p style=\"font-size: 1.8vw; margin-left: 15vw; margin-top: 5vw;\">You are receiving this email because we received a password reset request for your account. </p>" +
                        "<p style=\"font-weight: bold; font-size: 2.9vw; margin-left: 15vw; margin-top: 0; margin-bottom: 0;\" >Your verification code is: " + otp + "</p>" +
                        "<p style=\"font-size: 1.8vw; margin-left: 15vw;margin-top: 1.2vw;\">If you did not reset your password, no further action required.</p>" +
                        "<p style=\"font-size: 1.8vw; margin-left: 15vw;margin-top: 1.2vw;\">Please do not share OTP code for anyone.</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw; margin-top: 5vw;\">Regards!</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw; margin-bottom: 0;\">Akashiccorp Support Team</p>" +
                        "<hr style=\"margin-left: 15vw; margin-right: 17vw;\">" +
                        "<p style=\"font-size:1.75vw ;margin-left: 15vw; margin-top: 0;\">Xin chào!</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw; margin-top: 5vw;\">Bạn nhận được email này bởi vì chúng tôi nhận được yêu cầu khôi phục mật khẩu cho </p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw;\">tài khoản của bạn.</p>" +
                        "<p style=\"font-weight: bold; font-size:2.9vw ;margin-left: 15vw; margin-top: 0; margin-bottom: 0;\">Mã xác thực của bạn là: " + otp + "</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw;\">Nếu bạn không khôi phục mật khẩu thì bỏ qua email này.</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw;\">Lưu ý không chia sẻ mã xác thực OTP cho bất cứ ai</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw; margin-top: 5vw; margin-bottom: 0;\">Trân trọng!</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw;\">Đội ngũ akashiccorp</p>";
            return body;
        }

        public static string RenderWelcomeEmail(string email)
        {
            var body =
                    "<p style:\"font-weight: bold;\">Cảm ơn quý khách đã đăng ký tài khoản tại akashiccorp.</p>" +
                    "<p>Thông tin tài khoản của quý khách như sau:</p>" +
                    "<table>" +
                    "<tr><td style:\"font-weight: bold;\">Email:</td><td>" + email + "</td></tr>" +
                    "</table>";
            return body;
        }

        public static string RenderOtpEmail(string email, string otp, EOTPActionType actionType)
        {
            var desEn = "";
            var desVi = "";
            switch (actionType)
            {
                case EOTPActionType.AccountRegister:
                    desEn = "A login request has been made from your username:";
                    desVi = "Đăng ký tài khoản mới từ email:";
                    break;
                case EOTPActionType.AccountForgotPassword:
                    desEn = "You are receiving this email because we received a password reset request for your account:";
                    desVi = "Bạn nhận được email này bởi vì chúng tôi nhận được yêu cầu khôi phục mật khẩu cho tài khoản của bạn:";
                    break;
                default:
                    break;
            }
            var body = "<p style=\"font-weight:bolder; font-size: 1.8vw; margin-left: 15vw; margin-top: 5vw;\">Hello!</p>" +
                        "<p style=\"font-size: 1.8vw; margin-left: 15vw; margin-top: 5vw;\">" + desEn +
                            "<span style=\"color: blue; text-decoration: underline;\">" + email + "</span> " +
                        "</p>" +
                        "<p style=\"font-weight: bold; font-size: 2.9vw; margin-left: 15vw; margin-top: 0; margin-bottom: 0;\">Your verification code is: " + otp + "</p>" +
                        "<p style=\" font-size: 1.8vw; margin-left: 15vw;margin-top: 1.2vw;\">Please do not share OTP code for anyone.</p>" +
                        "<p style=\" font-size: 1.8vw; margin-left: 15vw;margin-top: 1.2vw;\">If you are having any issues with your account, please don’t hesitate to contact us by email: <br> " +
                            "<span style=\"color: blue;text-decoration: underline;\">hotro@akashiccorp</span>" +
                        "</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw; margin-top: 5vw;\">Regards!</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw; margin-bottom: 0;\">akashiccorp Support Team</p>" +
                        "<hr style=\"margin-left: 15vw; margin-right: 17vw;\">" +
                        "<p style=\"font-weight: bold; font-size:1.75vw ;margin-left: 15vw; margin-top: 0;\">Xin chào!</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw; margin-top: 5vw;\">" + desVi +
                            "<span style=\"color: blue; text-decoration: underline;\">" + email + "</span> " +
                        "</p>" +
                        "<p style=\"font-weight: bold; font-size:2.9vw ;margin-left: 15vw; margin-top: 0; margin-bottom: 0;\">Mã xác thực của bạn là: " + otp + "</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw;\">Lưu ý không chia sẻ mã xác thực OTP cho bất cứ ai.</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw;\">Nếu có bất cứ vấn đề gì, hãy liên hệ với chúng tôi thông qua email:" +
                            "<span style=\"color: blue; text-decoration: underline;\">hotro@akashiccorp</span>" +
                        "</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw; margin-top: 5vw; margin-bottom: 0;\">Trân trọng!</p>" +
                        "<p style=\"font-size:1.8vw ;margin-left: 15vw;\">Đội ngũ akashiccorp</p>";
            return body;
        }
    }
    public class EmailService : IIdentityMessageService
    {
        public class MailHelper
        {
            private readonly string _host;
            private readonly string _pass;
            private readonly int _port;
            private string _sender;
            private readonly bool _ssl;
            private readonly string _user;
            private const int Timeout = 0x2bf20;

            public MailHelper(string host, int port, string email, string password, bool ssl)
            {
                _host = host;
                _port = port;
                _user = email;
                _pass = password;
                _ssl = ssl;
                _sender = _user;
            }

            public void Send()
            {
                try
                {
                    Attachment item = null;
                    //MailMessage message = new MailMessage(Sender, Recipient, Subject, Body)
                    //{
                    //    IsBodyHtml = true
                    //};
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress(Sender, "akashiccorp");
                    message.To.Add(Recipient);
                    message.Subject = Subject;
                    message.Body = Body;
                    message.IsBodyHtml = true;

                    if (RecipientCC != null)
                    {
                        message.Bcc.Add(RecipientCC);
                    }
                    SmtpClient client = new SmtpClient(_host, _port);
                    if (!string.IsNullOrEmpty(AttachmentFile) && System.IO.File.Exists(AttachmentFile))
                    {
                        item = new Attachment(AttachmentFile);
                        message.Attachments.Add(item);
                    }
                    if ((_user.Length > 0) && (_pass.Length > 0))
                    {
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential(_user, _pass);
                        client.EnableSsl = _ssl;
                    }
                    client.Send(message);
                    if (item != null)
                    {
                        item.Dispose();
                    }
                    message.Dispose();
                    client.Dispose();
                }
                catch (Exception exception)
                {
                    throw new Exception(exception.Message);
                }
            }

            public string AttachmentFile { get; set; }

            public string Body { get; set; }

            public string Recipient { get; set; }

            public string RecipientCC { get; set; }

            public string Sender
            {
                get
                {
                    return _sender;
                }
                set
                {
                    _sender = value;
                }
            }

            public string Subject { get; set; }
        }
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            string SMTP = "smtp.gmail.com";
            int Port = 587;
            bool SSL = true;
            string mailAccount = "akashiccorp@gmail.com";
            string mailPassword = "Fighting369";

            MailHelper mail = new MailHelper(SMTP, Port, mailAccount, mailPassword, SSL);
            mail.Recipient = message.Destination;
            mail.Subject = message.Subject;
            mail.Body = message.Body;
            mail.Send();

            return Task.FromResult(0);
        }
    }
}
